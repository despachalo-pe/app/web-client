import stampit from 'stampit';
import Service from '~/services/service';
import { READY } from '~/services/sync/order-status';

const OrderService = stampit.methods({
  getOrdersPage({ search = '', page = 0, pageSize = 10, state = READY }) {
    return this.axios.$get('/sync/orders', {
      params: {
        search,
        page,
        pageSize,
        state,
      },
    });
  },
  getAllOrders({ state = READY }) {
    return this.axios.$get('/sync/orders/all', {
      params: {
        state,
      },
    });
  },
});

export default stampit(Service, OrderService);
