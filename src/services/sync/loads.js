import stampit from 'stampit';
import Service from '~/services/service';
import { ALL } from '~/services/sync/load-status';

const LoadService = stampit.methods({
  getLoadsPage({ search = '', page = 0, pageSize = 10, state = ALL }) {
    return this.axios.$get('/sync/loads', {
      params: {
        search,
        page,
        pageSize,
        state,
      },
    });
  },
  registerLoadFile({ file }) {
    const formData = new FormData();
    formData.append('file', file);
    return this.axios.$post('/sync/loads', formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    });
  },
  async getLoad({ loadId }) {
    const { load, ...rest } = await this.axios.$get(`/sync/loads/${loadId}`);
    return { ...load, ...rest };
  },
});

export default stampit(Service, LoadService);
