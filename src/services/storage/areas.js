import stampit from 'stampit';
import Service from '~/services/service';

const AreaService = stampit.methods({
  getAreasPage({ search = '', page = 0, pageSize = 10 }) {
    return this.axios.$get('/storage/areas', {
      params: {
        search,
        page,
        pageSize,
      },
    });
  },
  getArea({ areaId }) {
    return this.axios.$get(`/storage/areas/${areaId}`);
  },
  registerArea({ description, totalCapacity, centerId }) {
    return this.axios.$post('/storage/areas', {
      description,
      totalCapacity,
      centerId,
    });
  },
});

export default stampit(Service, AreaService);
