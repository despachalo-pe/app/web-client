import stampit from 'stampit';
import Service from '~/services/service';

const ShipmentService = stampit.methods({
  getShipmentsPage({ search = '', page = 0, pageSize = 10 }) {
    return this.axios.$get('/storage/shipments', {
      params: {
        search,
        page,
        pageSize,
      },
    });
  },
  getShipment({ shipmentId }) {
    return this.axios.$get(`/storage/shipments/${shipmentId}`);
  },
  confirmItems({ orderIds, zoneId, centerId, shipmentId }) {
    return this.axios.$post(`/storage/shipments/${shipmentId}/items`, {
      orderIds,
      areaId: zoneId,
      centerId,
    });
  },
});

export default stampit(Service, ShipmentService);
