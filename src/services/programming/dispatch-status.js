
export const LATE = 'ATRASADO';
export const PENDING = 'PENDIENTE';
export const CONFIRMED = 'CONFIRMADO';

export default [
  { text: LATE, value: LATE },
  { text: PENDING, value: PENDING },
  { text: CONFIRMED, value: CONFIRMED },
];
