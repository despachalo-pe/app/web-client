import stampit from 'stampit';

const Service = stampit({
  props: {
    axios: null,
  },
  init({ axios }) {
    this.axios = axios;
  },
});

export default Service;
