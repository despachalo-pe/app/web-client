import findDescendantsAndSelf from '~/helpers/pages';

describe('Test descendants and self for pages', () => {
  const pages = {
    a: { parent: null },
    b: { parent: 'a' },
  };

  test('Ok when get only self', () => {
    const pageId = 'a';
    expect(findDescendantsAndSelf(pageId, pages).length).toBe(1);
  });

  test('Ok when get nothing', () => {
    const pageId = null;
    expect(findDescendantsAndSelf(pageId, pages).length).toBe(0);
  });

  test('Ok when get descendants and self', () => {
    const pageId = 'b';
    expect(findDescendantsAndSelf(pageId, pages).length).toBe(2);
  });

  test('Throws when get descendant not found', () => {
    const pageId = 'c';
    const toThrows = () => findDescendantsAndSelf(pageId, pages);
    expect(toThrows).toThrow("Page with id 'c' not found");
  });
});
