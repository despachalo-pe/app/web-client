import axios from 'axios';
import { injectTokenInRequest } from '~/helpers/requests';

test('Ok when inject token in fetch api', () => {
  const fetchApi = axios.create();
  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
  injectTokenInRequest(fetchApi, token);
});

test('Ok when inject null token in fetch api', () => {
  const fetchApi = axios.create();
  const token = null;
  injectTokenInRequest(fetchApi, token);
});
