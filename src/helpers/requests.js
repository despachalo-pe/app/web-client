import { isNil } from 'ramda';

export const injectTokenInRequest = (fetchApi, token) => {
  fetchApi.interceptors.request.use((request) => {
    if (!isNil(token)) {
      request.headers.common.Authorization = 'Bearer ' + token;
    }

    return request;
  });
};
