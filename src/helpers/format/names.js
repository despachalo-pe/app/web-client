
export function fullName({ names = '', lastnames = '' } = {}) {
  return `${names} ${lastnames}`;
}
