import { prop } from 'ramda';

function findDescendantsAndSelf(pageId, pages) {
  if (!pageId) {
    return [];
  }

  const self = prop(pageId, pages);

  if (!self) {
    throw new Error(`Page with id '${pageId}' not found`);
  }

  if (!self.parent) {
    return [self];
  }

  const descendants = findDescendantsAndSelf(self.parent, pages);
  return [...descendants, self];
}

export default findDescendantsAndSelf;
