import UserService from '~/services/company/users';

export const state = () => ({
  users: [],
  pageInfo: {},
});

export const actions = {
  async getUsers({ commit }, { search = '', page = 0, pageSize = 10 }) {
    const service = this.$createService(UserService);
    try {
      const usersPage = await service.getUsersPage({ search, page, pageSize });
      commit('storeUsersPage', usersPage);
    } catch (e) {
      console.error(e);
      throw new Error('No se pudo obtener los usuarios');
    }
  },
};

export const mutations = {
  storeUsersPage(_state, { data, page, pageSize, total }) {
    _state.users = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
};
