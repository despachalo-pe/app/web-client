import { indexBy, prop, values as ramdaValues } from 'ramda';
import CenterService from '~/services/company/centers';

export const state = () => ({
  current: {
    center: {},
  },
  centers: [],
  pageInfo: {},
  allCenters: {},
});

export const actions = {
  async getCenters({ commit }, { search = '', page = 0, pageSize = 10 }) {
    const service = this.$createService(CenterService);
    try {
      const centersPage = await service.getCentersPage({ search, page, pageSize });
      commit('storeCentersPage', centersPage);
    } catch (e) {
      throw new Error('No se pudo obtener los centros de distribución');
    }
  },
  async registerNewCenter({ commit }, { name, address, location }) {
    const service = this.$createService(CenterService);
    try {
      await service.registerCenter({ name, address, location });
    } catch (e) {
      console.error(e);
      throw new Error('No se pudo registrar el centro de distribución');
    }
  },
  async getCenterDetails({ commit }, { centerId }) {
    const service = this.$createService(CenterService);
    try {
      const center = await service.getCenter({ centerId });
      commit('storeCurrentCenter', center);
    } catch (e) {
      throw new Error('No se pudo obtener el centro de distribución');
    }
  },
  async getAllCenters({ commit }) {
    const service = this.$createService(CenterService);
    try {
      const centers = await service.getAll();
      commit('storeAllCenters', centers);
    } catch (e) {
      throw new Error('No se pudo obtener todos los centros de distribución');
    }
  },
};

export const getters = {
  all: _state => ramdaValues(_state.allCenters),
  fromId: _state => id => _state.allCenters[id],
};

export const mutations = {
  storeCentersPage(_state, { data, page, pageSize, total }) {
    _state.centers = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
  storeCurrentCenter(_state, center) {
    _state.current.center = center;
  },
  storeAllCenters(_state, centers) {
    _state.allCenters = indexBy(prop('id'), centers ?? []);
  },
};
