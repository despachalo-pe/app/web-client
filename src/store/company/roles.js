import RoleService from '~/services/company/roles';

export const state = () => ({
  roles: [],
  pageInfo: {},
});

export const actions = {
  async getRoles({ commit }, { search = '', page = 0, pageSize = 10 }) {
    const service = this.$createService(RoleService);
    try {
      const rolesPage = await service.getRolesPage({ search, page, pageSize });
      commit('storeRolesPage', rolesPage);
    } catch (e) {
      console.error(e);
      throw new Error('No se pudo obtener los roles');
    }
  },
};

export const mutations = {
  storeRolesPage(_state, { data, page, pageSize, total }) {
    _state.roles = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
};
