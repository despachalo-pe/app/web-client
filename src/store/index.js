
export const actions = {
  async nuxtServerInit({ dispatch }) {
    if (this.$auth.loggedIn) {
      await dispatch('company/centers/getAllCenters');
    }
  },
};
